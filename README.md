# Android CI/CD project
This is a sample project for Android CI/CD configuration and a pipeline for:
- Build
- Test
- Relase build
- Sign
- Upload to Dropbox
- Send email with download link and latest changes

This project is described in details in this [Medium article](https://medium.com/p/android-gitlab-ci-cd-sign-deploy-3ad66a8f24bf?source=email-25f5da2347f4--writer.postDistributed&sk=6170eb42c2aff99993bbd71694e7c222) 
and also in [my GitHub pages](https://mega-arbuz.github.io/2019/02/14/android-ci-cd-with-gitlab-build-test-sign-upload-to-dropbox-and-send-an-email.html).